package cn.superfw.domain.repository;

import cn.superfw.domain.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface OrderRepositoryDsl extends JpaRepository<Order, Long>, QueryDslPredicateExecutor<Order> {
}
