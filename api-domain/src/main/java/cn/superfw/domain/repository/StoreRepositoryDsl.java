package cn.superfw.domain.repository;

import cn.superfw.domain.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface StoreRepositoryDsl extends JpaRepository<Store, Long>, QueryDslPredicateExecutor<Store> {
}
