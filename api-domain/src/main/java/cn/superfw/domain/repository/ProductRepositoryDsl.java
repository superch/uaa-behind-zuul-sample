package cn.superfw.domain.repository;

import cn.superfw.domain.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface ProductRepositoryDsl extends JpaRepository<Product, Long>, QueryDslPredicateExecutor<Product> {
}
