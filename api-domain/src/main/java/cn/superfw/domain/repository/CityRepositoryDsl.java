package cn.superfw.domain.repository;

import cn.superfw.domain.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface CityRepositoryDsl extends JpaRepository<City, Integer>,QueryDslPredicateExecutor<City> {
}
