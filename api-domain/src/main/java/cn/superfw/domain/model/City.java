package cn.superfw.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "t_city")
public class City implements Serializable {
    private static final long serialVersionUID = 1L;

    public City() {
    }

    public City(String cityCode, String cityName, String cityDes) {
        this.cityCode = cityCode;
        this.cityName = cityName;
        this.cityDes = cityDes;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String cityCode;
    private String cityName;
    private String cityDes;

}
