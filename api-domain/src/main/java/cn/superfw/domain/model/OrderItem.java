package cn.superfw.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "t_order_item")
public class OrderItem extends BaseEntity {

    public OrderItem() {
    }

    public OrderItem(Order order) {
        this.order = order;
    }

    /**
     * 订单
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    /**
     * 商品编号
     */
    private Long productId;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品价格
     */
    private BigDecimal productPrice;

    /**
     * 商品型号描述
     */
    private String productModeDesc;

    /**
     * 折扣比例
     */
    private BigDecimal discountRate;

    /**
     * 折扣金额
     */
    private BigDecimal discountAmount;

    /**
     * 购买数量
     */
    private Integer number;

    /**
     * 小计金额
     */
    private BigDecimal subtotal;

}
