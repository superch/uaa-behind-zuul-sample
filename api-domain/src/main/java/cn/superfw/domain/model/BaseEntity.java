package cn.superfw.domain.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    protected Long commonId;

    private String createdBy;
    private String updatedBy;
    private Timestamp createdTime;
    private Timestamp lastUpdateTime;

}
