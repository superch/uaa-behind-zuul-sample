package cn.superfw.domain.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_store")
public class Store extends BaseEntity {

    public Store() {
    }

    public Store(String storeNo, String storeName) {
        this.storeNo = storeNo;
        this.storeName = storeName;
    }

    public void setStoreDetails(StoreDetails storeDetails) {
        this.storeDetails = storeDetails;
        this.storeDetails.setStore(this);
    }

    /**
     * 店铺编号
     */
    private String storeNo;

    /**
     * 店铺名称
     */
    private String storeName;

    /**
     * 店铺详情
     */
    @OneToOne(mappedBy = "store", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private StoreDetails storeDetails;
}
