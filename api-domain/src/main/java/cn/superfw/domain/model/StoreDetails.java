package cn.superfw.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_store_details")
public class StoreDetails extends BaseEntity {

    public StoreDetails() {
    }

    public StoreDetails(String storeRegistrant, String storeRegistrantPhone) {
        this.storeRegistrant = storeRegistrant;
        this.storeRegistrantPhone = storeRegistrantPhone;
    }

    /**
     * 店铺
     */
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id")
    private Store store;

    /**
     * 店铺注册人
     */
    private String storeRegistrant;

    /**
     * 店铺注册人电话
     */
    private String storeRegistrantPhone;
}
