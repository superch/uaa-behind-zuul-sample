package cn.superfw.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "t_order")
public class Order extends BaseEntity {
    /**
     * 订单单号
     */
    private String orderNo;

    /**
     * 店铺
     */
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "store_id")
    private Store store;

    /**
     * 订单条目
     */
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderItem> orderItems = new ArrayList<>();

    /**
     * 订单状态
     */
    private Integer orderStatus;

    /**
     * 商品数量
     */
    private Integer productCount;

    /**
     * 商品总价
     */
    private BigDecimal productAmountTotal;

    /**
     * 订单金额
     */
    private BigDecimal orderAmountTotal;

    /**
     * 订单支付渠道
     */
    private Integer payChannel;

    /**
     * 订单支付单号
     */
    private String outTradeNo;

    /**
     * 客户编号
     */
    private String userId;

}
