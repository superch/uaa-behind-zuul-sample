package cn.superfw.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "ApiResource", description = "Api资源统一封装类")
public class ApiResource<T extends Object> {

    @ApiModelProperty("返回代码")
    private String code;

    @ApiModelProperty("返回消息")
    private String message;

    @ApiModelProperty("返回结果内容")
    private T result;

    public ApiResource(Builder<T> builder) {
        this.code = builder.code;
        this.message = builder.message;
        this.result = builder.result;
    }

    public ApiResource() {
    }

    public ApiResource(String code, String message, T result) {
        this.code = code;
        this.message = message;
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public static class Builder<T> {
        private String code;
        private String message;
        private T result;

        public Builder coce(String code) {
            this.code = code;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder result(T result) {
            this.result = result;
            return this;
        }

        public ApiResource build() {
            return new ApiResource(this);
        }

    }
}
